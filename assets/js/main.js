document.addEventListener('DOMContentLoaded', async function() {
    const token = localStorage.getItem('token');

    // Check if token exists in localStorage and redirect if user is already logged in
    if (token) {
        const currentPath = window.location.pathname;
        if (currentPath === '/index.html' || currentPath === '/login.html') {
            window.location.href = '/courses.html'; // Redirect to a logged-in page
            return;
        }
    } else {
        // Redirect to login.html if token does not exist and current path is not login.html or index.html
        const currentPath = window.location.pathname;
        if (currentPath !== '/login.html' && currentPath !== '/index.html') {
            window.location.href = '/login.html';
            return;
        }
    }

    // Function to populate the course select options
    function populateCourseOptions(courses) {
        const courseSelect = document.getElementById('course');
        if (!courseSelect) {
            // console.log('Course select element not found.');
            return;
        }

        courseSelect.innerHTML = '<option value="" disabled selected>Choisissez un cours</option>'; // Reset options

        courses.forEach(course => {
            const option = document.createElement('option');
            option.value = course.id;
            option.textContent = course.titre;
            courseSelect.appendChild(option);
        });
    }

    // Function to display courses in the DOM
    function displayCourses(courses) {
        const container = document.getElementById('course-cards-container');
        // Ensure that the container element to hold courses is exists 
        if (!container) {
            // console.log('Course cards container element not found.');
            return;
        }

        container.innerHTML = ''; // Clear existing content

        courses.forEach(course => {
            const courseCard = document.createElement('div');
            courseCard.className = 'col-12 col-md-4 col-lg-3 mt-md-3';
            courseCard.innerHTML = `
                <div class="card">
                    <img src="${course.image || 'https://via.placeholder.com/300x200'}" class="card-img-top" alt="Course Image">
                    <div class="card-body">
                        <h5 class="card-title">${course.titre}</h5>
                        <p class="card-text m-0">Description: <span class='fw-light'>${course.description.substring(0, 23)}...</span></p>
                        <p class="card-text m-0">Date: <span class='fw-light'>${course.date}</span></p>
                        <p class="card-text m-0">Heure: <span class='fw-light'>${course.heure}</span></p>
                        <p class="card-text m-0 mb-2">Durée: <span class='fw-light'>${course.durée} min</span></p>
                    </div>
                </div>
            `;
            container.appendChild(courseCard);
        });
    }

    // Function to fetch courses from API
    async function fetchCourses() {
        try {
            const response = await fetch('http://localhost:8000/api/courses', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }

            const data = await response.json();
            if (!data || !data.data || !Array.isArray(data.data)) {
                console.log('Invalid API response structure');
            }

            const courses = data.data;

            // Display courses in the DOM
            displayCourses(courses);

            // Populate the select options
            populateCourseOptions(courses);

            return courses;

        } catch (error) {
            console.log('Error fetching courses:', error);
        }
    }
    
    // Function to fetch user bookings
    async function fetchUserBookings(userId) {
        try {
            const response = await fetch(`http://localhost:8000/api/bookings/${userId}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            if (!response.ok) {
                console.error(`HTTP error! Status: ${response.status}`);
                return null;
            }

            const bookingsData = await response.json();
            return bookingsData.data; // Assuming API returns data in 'data' field
        } catch (error) {
            console.error('Error fetching user bookings:', error);
            return null;
        }
    }

    // Function to render bookings in the table
    function renderBookings(bookings, courses) {
        const tableBody = document.getElementById('bookingTableBody');
        if (!tableBody) {
            console.error('Booking table body element not found.');
            return;
        }
        tableBody.innerHTML = ''; // Clear existing rows
        
        bookings.forEach(booking => {
            const course = courses.find(course => course.id === booking.course_id);
            const courseName = course ? course.titre : 'Unknown Course';

            const row = `
                <tr>
                    <th scope="row">${booking.id}</th>
                    <td>${booking.date_de_reservation}</td>
                    <td>${courseName}</td>
                    <td>
                        ${booking.status === 'pending' 
                            ? '<span class="badge text-bg-warning fw-normal">' + booking.status.charAt(0).toUpperCase() + booking.status.slice(1) + '</span>' 
                            : '<span class="badge text-bg-success fw-normal">' + booking.status.charAt(0).toUpperCase() + booking.status.slice(1) + '</span>'
                        }
                    </td>
                </tr>
            `;
            tableBody.innerHTML += row;
        });
    }

    // Load user ID from localStorage or elsewhere
    const userId = localStorage.getItem('user_id');
    if (userId) {
        // Fetch courses and bookings
        const courses = await fetchCourses();
        const bookings = await fetchUserBookings(userId);
        if (courses && bookings) {
            renderBookings(bookings, courses); // Render bookings with course names in the table
        } else {
            console.error('No courses or bookings data returned.');
        }
    } else {
        console.error('User ID not found.');
    }

    // Function to handle form submission
    async function handleReservation(event) {
        event.preventDefault();
        const form = event.target;
        const courseSelect = form.querySelector('#course');
        const userId = localStorage.getItem('user_id');
        const reservationMessage = document.querySelector('.reservationErrorMessage');
    
        if (!courseSelect.value) {
            showMessageError(reservationMessage, 'Please select a course', 'red');
            return;
        }
    
        try {
            // Fetch the selected course details
            const courseId = courseSelect.value;
            const courseResponse = await fetch(`http://localhost:8000/api/course/${courseId}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
    
            if (!courseResponse.ok) {
                const errorText = await courseResponse.text();
                console.log(`Failed to fetch course details. HTTP error! Status: ${courseResponse.status}, Response: ${errorText}`);
            }
    
            const courseData = await courseResponse.json();
            const courseDate = courseData.data.date;
    
            // Prepare payload for reservation
            const payload = {
                user_id: userId,
                course_id: courseId,
                date_de_reservation: courseDate
            };
    
            // Make reservation request
            const reservationResponse = await fetch('http://localhost:8000/api/bookings', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            });
    
            if (!reservationResponse.ok) {
                const errorData = await reservationResponse.json();
                showMessageError(reservationMessage, errorData.message, 'red')
            }
    
            const responseData = await reservationResponse.json();
            // console.log('Reservation successful:', responseData);
            showMessageError(reservationMessage, 'Reservation successful...', 'green')
    
        } catch (error) {
            console.log('Error making reservation:', error);
            // showMessageError(reservationMessage, 'Failed to make a reservation. Please try again.');
        }
    }
    
    // Add event listener for reservation form
    const reservationForm = document.getElementById('reservationForm');
    if (reservationForm) {
        reservationForm.addEventListener('submit', handleReservation);
    }


    // Function to handle logout
    async function handleLogout() {
        try {
            const response = await fetch('http://localhost:8000/api/logout_user', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            });

            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }

            localStorage.removeItem('token');
            localStorage.removeItem('user_id');
            const currentPath = window.location.pathname;
            if (currentPath !== '/login.html' && currentPath !== '/index.html') {
                window.location.href = '/login.html';
                return;
            }

        } catch (error) {
            console.log('Error logging out:', error);
        }
    }

    // Add event listener for logout button
    const logoutButton = document.getElementById('logoutButton');
    if (logoutButton) {
        logoutButton.addEventListener('click', handleLogout);
    }

    // Register user form
    const registerForm = document.querySelector('#registerForm');
    if (registerForm) {
        const registerMessage = document.querySelector('.registerErrorMessage');
        const nomMsgError = document.querySelector('.nomMsgError');
        const prénomMsgError = document.querySelector('.prénomMsgError');
        const emailMsgError = document.querySelector('.emailMsgError');
        const passwordMsgError = document.querySelector('.passwordMsgError');

        registerForm.addEventListener('submit', async function(event) {
            event.preventDefault();

            const formData = new FormData(registerForm);
            const url = 'http://localhost:8000/api/users'; // Replace with your backend endpoint

            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                    },
                    body: formData
                });

                const responseData = await response.json();

                if (!response.ok) {
                    if (responseData.errors) {
                        // Handle validation errors
                        Object.keys(responseData.errors).forEach(function(key) {
                            const errorMessages = responseData.errors[key].join(', ');
                            const inputField = document.getElementById(key);
                            if (inputField) {
                                inputField.classList.add('is-invalid');
                                const errorElement = document.createElement('div');
                                errorElement.className = 'invalid-feedback';
                                errorElement.innerText = errorMessages;
                                inputField.parentNode.appendChild(errorElement);
                            }
                        });
                    } else {
                        // Handle other errors
                        console.log('Error:', responseData.data);
                        const nom = responseData.data.nom;
                        const prénom = responseData.data.prénom;
                        const email = responseData.data.email;
                        const password = responseData.data.password;

                        if (responseData.data.nom) {
                            showMessageError(nomMsgError, nom, 'red');
                        }
                        if (responseData.data.prénom) {
                            showMessageError(prénomMsgError, prénom, 'red');
                        }
                        if (responseData.data.email) {
                            showMessageError(emailMsgError, email, 'red');
                        }
                        if (responseData.data.password) {
                            showMessageError(passwordMsgError, password, 'red');
                        }
                    }
                } else {
                    // Successful registration, handle redirection or success message
                    console.log('User registered successfully:', responseData);
                    showMessageError(registerMessage, 'User registered successfully' ,'green')
                    setTimeout(() => {
                        window.location.href = '/login.html'; // Redirect to login page
                    }, 3000);
                }
            } catch (error) {
                console.log('Error:', error);
                registerMessage.innerHTML = `Error: <span style="color:red">${error.message}</span>`;
            }
        });
    }

    // Login user form
    const loginForm = document.querySelector('#loginForm');
    if (loginForm) {
        const loginMessage = document.querySelector('.loginErrorMessage');
        const emailLoginMsgError = document.querySelector('.emailMsgError');
        const passwordLoginMsgError = document.querySelector('.passwordMsgError');

        loginForm.addEventListener('submit', async function(event) {
            event.preventDefault();

            const formData = new FormData(loginForm);
            const url = 'http://localhost:8000/api/login_user'; // Replace with your backend login endpoint

            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                    },
                    body: formData
                });

                const responseData = await response.json();

                if (!response.ok) {
                    if (responseData.errors) {
                        // Handle validation errors
                        Object.keys(responseData.errors).forEach(function(key) {
                            const errorMessages = responseData.errors[key].join(', ');
                            const inputField = document.getElementById(key);
                            if (inputField) {
                                inputField.classList.add('is-invalid');
                                const errorElement = document.createElement('div');
                                errorElement.className = 'invalid-feedback';
                                errorElement.innerText = errorMessages;
                                inputField.parentNode.appendChild(errorElement);
                            }
                        });
                    } else {
                        // Handle other errors
                        console.log('Error:', responseData.data);
                        const email = responseData.data.email;
                        const password = responseData.data.password;

                        if (responseData.data.email) {
                            showMessageError(emailLoginMsgError, email, 'red');
                        }
                        if (responseData.data.password) {
                            showMessageError(passwordLoginMsgError, password, 'red');
                        }
                    }
                } else {
                    // Successful login, handle redirection or success message
                    console.log('User logged successfully:', responseData);
                    if (responseData.data.token) {
                        localStorage.setItem('token', responseData.data.token);
                        localStorage.setItem('user_id', responseData.data.user.id);
                        console.log('Token saved to localStorage:', responseData.data.token);
                        // Redirect to courses page or another page after login
                        window.location.href = '/courses.html';
                    } else {
                        console.log('Token not found in response');
                    }
                }
            } catch (error) {
                console.log('Error:', error);
                loginMessage.innerHTML = `Error: <span style="color:red">${error.message}</span>`;
            }
        });
    }

    // ------------------------------
    // Confirmation booking logic
    // ------------------------------

    // Function to check if the user is authenticated
    function isAuthenticated() {
        return token !== null;
    }

    // Function to confirm the booking
    function confirmBooking(bookingId) {
        const confirmErrorMessage=document.querySelector('.confirmErrorMessage')
        if (!isAuthenticated()) {
            showMessageError(confirmErrorMessage, 'You must be logged in to confirm a booking.', 'red')
            return;
        }

        fetch(`http://localhost:8000/api/bookings/${bookingId}/confirm`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
        })
        .then(response => response.json())
        .then(data => {
            if (data.status === 'success') {
                showMessageError(confirmErrorMessage, 'Booking confirmed successfully.', 'green')
                setTimeout(() => {
                    window.location.href = 'user_reservations.html';
                }, 3000);

            } else {
                showMessageError(confirmErrorMessage, `Failed to confirm booking: ${data.message}`, 'red')
            }
        })
        .catch(error => {
            console.error('Error:', error);
            showMessageError(confirmErrorMessage, 'An error occurred while confirming the booking.', 'red')
        });
    }

    // Get the booking ID from the query parameters
    const params = getQueryParams();
    const bookingId = params.booking_id;

    // Confirm the booking if the booking ID is present
    if (bookingId) {
        confirmBooking(bookingId);
    } else {
        showMessageError(confirmErrorMessage, 'Invalid booking ID.', 'red')
        showMessageError(confirmErrorMessage, 'An error occurred while confirming the booking.', 'red')
    }

    // Fetch and display courses on page load
    fetchCourses();

});

// Function to show flash message
function showMessageError(element, message, color) {
    element.innerHTML = `<span style="color:${color};font-size:15px;font-weight: bold;margin:0px;padding:0px;">${message}</span>`;
    setTimeout(() => {
        element.innerHTML = '';
    }, 3000);
}

// Function to extract query parameters from the URL
function getQueryParams() {
    const params = {};
    window.location.search.substring(1).split('&').forEach(param => {
        const [key, value] = param.split('=');
        params[key] = decodeURIComponent(value);
    });
    return params;
}